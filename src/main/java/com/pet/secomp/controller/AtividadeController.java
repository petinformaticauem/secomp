package com.pet.secomp.controller;

import com.pet.secomp.model.evento.Atividade;
import com.pet.secomp.repository.AtividadeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/atividades")

public class AtividadeController {

	private final AtividadeRepository atividadeRepository;

	@Autowired
	public AtividadeController(AtividadeRepository atividadeRepository) {
		this.atividadeRepository = atividadeRepository;
	}

	@GetMapping
	public String atividades(Model model) {
		// TODO: Lista de atividades.
		model.addAttribute("atividades", atividadeRepository.findAll());
		return "/atividades/list";
	}

	@GetMapping("/{id}")
	public String atividade(@PathVariable int id, Model model) {
		// TODO: Página da atividade.
		return "get_single_atividade";
	}

	@GetMapping("/create")
	public String getCreate(Model model) {
		// TODO: Página de criação.
		model.addAttribute("atividade", new Atividade());
		return "/atividades/create_atividade";
	}

	@PostMapping
	public String postCreate(Atividade atividade) {
		// TODO: Endpoint de criação.
		atividadeRepository.save(atividade);
		return "redirect:/atividades/list";
	}

	@GetMapping("/{id}/update")
	public String getUpdate(@PathVariable int id, Model model) {
		// TODO: Página de modificação.
		return "update_atividade";
	}

	@PutMapping("/{id}")
	public String putUpdate(@PathVariable int id) {
		// TODO: Endpoint de modificação.
		return "redirect:/atividades/" + id;
	}

	@GetMapping("/{atividadeId}/inscricao")
	public String getInscricao(@PathVariable int atividadeId, Model model) {
		// TODO: Página de inscricao da atividade.
		return "get_single_inscricao";
	}

	@PostMapping("/{atividadeId}/inscricao")
	public String postInscricao(@PathVariable int atividadeId) {
		// TODO: Endpoint de criação inscricao da atividade.
		return "redirect:/atividades/" + atividadeId;
	}

	@PutMapping("/{atividadeId}/inscricao/{inscricaoId}")
	public String putInscricao(@PathVariable int atividadeId, @PathVariable int inscricaoId) {
		// TODO: Endpoint de modificação de inscricao da atividade.
		return "redirect:/atividades/" + atividadeId;
	}

	@GetMapping("/{atividadeId}/presenca")
	public String getPresencas(@PathVariable int atividadeId, @PathVariable int presencaId, Model model) {
		// TODO: Lista de presencas da atividade.
		return "get_all_presencas";
	}

	@GetMapping("/{atividadeId}/presenca/{presencaId}")
	public String getPresenca(@PathVariable int atividadeId, @PathVariable int presencaId, Model model) {
		// TODO: Página de presenca da atividade.
		return "get_single_presenca";
	}

	@PostMapping("/{atividadeId}/presenca/{presencaId}")
	public String postPresenca(@PathVariable int atividadeId, @PathVariable int presencaId) {
		// TODO: Endpoint de presenca da atividade.
		return "redirect:/atividades/" + atividadeId + "/presenca/" + presencaId;
	}
}
