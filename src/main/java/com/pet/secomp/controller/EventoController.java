package com.pet.secomp.controller;

import com.pet.secomp.model.evento.Evento;
import com.pet.secomp.repository.EventoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/eventos")
public class EventoController {

	private final EventoRepository eventoRepository;

	@Autowired
	public EventoController(EventoRepository eventoRepository) {
		this.eventoRepository = eventoRepository;
	}

	@ModelAttribute("allEventos")
	public List<Evento> populateEventos() {
		return this.eventoRepository.findAll();
	}

	@GetMapping
	public String eventos() {
		// TODO: Lista de eventos.
		return "eventos/get_all_eventos";
	}

	@GetMapping("/{id}")
	public String evento(@PathVariable Long id, Model model) {
		// TODO: Página do evento.
		return "get_single_evento";
	}

	@GetMapping("/create")
	public String getCreate() {
		// TODO: Página de criação.
		return "create_evento";
	}

	@PostMapping
	public String postCreate() {
		// TODO: Endpoint de criação.
		return "redirect:/eventos";
	}

	@GetMapping("/{id}/update")
	public String getUpdate(@PathVariable Long id, Model model) {
		// TODO: Página de modificação.
		return "update_evento";
	}

	@PutMapping("/{id}")
	public String putUpdate(@PathVariable Long id) {
		// TODO: Endpoint de modificação.
		return "redirect:/eventos/" + id;
	}
}
