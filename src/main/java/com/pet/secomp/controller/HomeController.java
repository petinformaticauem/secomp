package com.pet.secomp.controller;

import com.pet.secomp.requests.Mensagem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping({"/", "/index.html", "/index"})
public class HomeController {

	private final JavaMailSender mailSender;

	@Autowired
	public HomeController(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}

	@GetMapping
	public String index() {
		return "index";
	}

	@GetMapping("/apresentacao")
	public String apresentacao() {
		return "apresentacao";
	}

	@GetMapping("/programacao")
	public String programacao() {
		return "programacao";
	}

	@GetMapping("/contato")
	public String contato(Model model) {
		model.addAttribute("mensagem", new Mensagem());
		return "contato";
	}

	@PostMapping("/contact")
	public String email(Mensagem msg, final RedirectAttributes ra) throws Exception {
		SimpleMailMessage mail = new SimpleMailMessage();
		mail.setTo("pet@din.uem.br");
		mail.setSubject("SECOMP");
		mail.setText("Nome: " + msg.getNome() + "\n" +
				"E-mail: " + msg.getEmail() + "\n" +
				"Mesagem: " + msg.getMessage() + "\n");
		mailSender.send(mail);
		ra.addFlashAttribute("info", "Mensagem enviada com sucesso!");
		return "redirect:/contato";
	}
}