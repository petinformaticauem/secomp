package com.pet.secomp.controller;

import com.pet.secomp.repository.PessoaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/users")
public class UsuarioController {

	private final PessoaRepository pessoaRepository;

	@Autowired
	public UsuarioController(PessoaRepository pessoaRepository) {
		this.pessoaRepository = pessoaRepository;
	}

	@GetMapping
	public String users(Model model) {
		// TODO: Lista de usuários.
		return "get_all_users";
	}

	@GetMapping("/{id}")
	public String user(@PathVariable int id, Model model) {
		// TODO: Página do usuário.
		return "get_single_user";
	}

	@GetMapping("/create")
	public String getCreate() {
		// TODO: Página de criação.
		return "create_user";
	}

	@PostMapping
	public String postCreate() {
		// TODO: Endpoint de criação.
		return "redirect:/users";
	}

	@GetMapping("/{id}/update")
	public String getUpdate(@PathVariable int id, Model model) {
		// TODO: Página de modificação.
		return "update_user";
	}

	@PutMapping("/{id}")
	public String putUpdate(@PathVariable int id) {
		// TODO: Endpoint de modificação.
		return "redirect:/users/" + id;
	}
}
