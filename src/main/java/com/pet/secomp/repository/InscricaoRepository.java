package com.pet.secomp.repository;

import com.pet.secomp.model.evento.Inscricao;
import com.pet.secomp.model.evento.InscricaoPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InscricaoRepository extends JpaRepository<Inscricao, InscricaoPK> {
}
