package com.pet.secomp.repository;

import com.pet.secomp.model.evento.Periodo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PeriodoRepository extends JpaRepository<Periodo, Integer> {
}
