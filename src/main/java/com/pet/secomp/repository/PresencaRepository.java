package com.pet.secomp.repository;

import com.pet.secomp.model.evento.Presenca;
import com.pet.secomp.model.evento.PresencaPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PresencaRepository extends JpaRepository<Presenca, PresencaPK> {
}
