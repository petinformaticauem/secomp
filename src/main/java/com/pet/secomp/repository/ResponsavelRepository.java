package com.pet.secomp.repository;

import com.pet.secomp.model.evento.Responsavel;
import com.pet.secomp.model.evento.ResponsavelPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ResponsavelRepository extends JpaRepository<Responsavel, ResponsavelPK> {
}
