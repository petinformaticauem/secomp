package com.pet.secomp.repository;

import com.pet.secomp.model.Petiano;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PetianoRepository extends JpaRepository<Petiano, Integer> {
}
