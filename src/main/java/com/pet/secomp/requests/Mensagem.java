package com.pet.secomp.requests;

import java.io.Serializable;

public class Mensagem implements Serializable {
	private String nome;
	private String email;
	private String message;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}