package com.pet.secomp.model;

import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "Pessoa")
public class Pessoa {

	@Id
	@Column(name = "pessoa_id", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@NotNull
	@Basic(optional = false)
	@Column(name = "pessoa_email", nullable = false, unique = true)
	private String email;

	@NotNull
	@Basic(optional = false)
	@Column(name = "pessoa_password", nullable = false)
	private String password;

	@NotNull
	@Basic(optional = false)
	@Column(name = "pessoa_rg", nullable = false, unique = true)
	private String rg;

	@CPF
	@NotNull
	@Basic(optional = false)
	@Column(name = "pessoa_cpf", nullable = false, unique = true)
	private String cpf;

	@NotNull
	@Basic(optional = false)
	@Column(name = "pessoa_nascimento", nullable = false)
	private Date nascimento;

	@NotNull
	@Basic(optional = false)
	@Column(name = "pessoa_phone", nullable = false)
	private String phone;

	@NotNull
	@Basic(optional = false)
	@Column(name = "pessoa_address", nullable = false)
	private String address;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public Date getNascimento() {
		return nascimento;
	}

	public void setNascimento(Date nascimento) {
		this.nascimento = nascimento;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Pessoa)) return false;

		Pessoa pessoa = (Pessoa) o;

		if (id != pessoa.id) return false;
		if (!email.equals(pessoa.email)) return false;
		if (!password.equals(pessoa.password)) return false;
		if (!rg.equals(pessoa.rg)) return false;
		if (!cpf.equals(pessoa.cpf)) return false;
		if (!nascimento.equals(pessoa.nascimento)) return false;
		if (!phone.equals(pessoa.phone)) return false;
		return address.equals(pessoa.address);
	}

	@Override
	public int hashCode() {
		int result = id;
		result = 31 * result + email.hashCode();
		result = 31 * result + password.hashCode();
		result = 31 * result + rg.hashCode();
		result = 31 * result + cpf.hashCode();
		result = 31 * result + nascimento.hashCode();
		result = 31 * result + phone.hashCode();
		result = 31 * result + address.hashCode();
		return result;
	}
}
