package com.pet.secomp.model.evento;

import javax.persistence.*;

@Entity
@Table(name = "Presenca",
		uniqueConstraints = @UniqueConstraint(
				columnNames = {"presenca_periodo_id", "presenca_inscricao_id"}
		)
)
@IdClass(PresencaPK.class)
public class Presenca {

	@Id
	@Column(name = "presenca_periodo_id", nullable = false)
	private int periodoId;

	@Id
	@Column(name = "presenca_inscricao_id", nullable = false)
	private int inscricaoId;

	public int getPeriodoId() {
		return periodoId;
	}

	public void setPeriodoId(int periodoId) {
		this.periodoId = periodoId;
	}

	public int getInscricaoId() {
		return inscricaoId;
	}

	public void setInscricaoId(int inscricaoId) {
		this.inscricaoId = inscricaoId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Presenca)) return false;

		Presenca presenca = (Presenca) o;

		if (periodoId != presenca.periodoId) return false;
		return inscricaoId == presenca.inscricaoId;
	}

	@Override
	public int hashCode() {
		int result = periodoId;
		result = 31 * result + inscricaoId;
		return result;
	}
}
