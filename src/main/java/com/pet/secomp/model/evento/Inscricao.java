package com.pet.secomp.model.evento;

import com.pet.secomp.model.enums.StatusInscricao;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "Inscricao")
@IdClass(InscricaoPK.class)
public class Inscricao {

	@Id
	@Column(name = "inscricao_atividade_id", nullable = false)
	private int atividadeId;

	@Id
	@Column(name = "inscricao_pessoa_id", nullable = false)
	private int pessoaId;

	@NotNull
	@Basic(optional = false)
	@Column(name = "inscricao_status", nullable = false)
	@Enumerated(EnumType.STRING)
	private StatusInscricao status;

	@Basic
	@Column(name = "inscricao_certificado")
	private String certificado;

	// TODO: Campo de presença
	//@Formula()
	//private int frequencia;

	public int getAtividadeId() {
		return atividadeId;
	}

	public void setAtividadeId(int atividadeId) {
		this.atividadeId = atividadeId;
	}

	public int getPessoaId() {
		return pessoaId;
	}

	public void setPessoaId(int pessoaId) {
		this.pessoaId = pessoaId;
	}

	public StatusInscricao getStatus() {
		return status;
	}

	public void setStatus(StatusInscricao status) {
		this.status = status;
	}

	public String getCertificado() {
		return certificado;
	}

	public void setCertificado(String certificado) {
		this.certificado = certificado;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Inscricao)) return false;

		Inscricao inscricao = (Inscricao) o;

		if (atividadeId != inscricao.atividadeId) return false;
		if (pessoaId != inscricao.pessoaId) return false;
		if (status != inscricao.status) return false;
		return certificado != null ? certificado.equals(inscricao.certificado) : inscricao.certificado == null;
	}

	@Override
	public int hashCode() {
		int result = atividadeId;
		result = 31 * result + pessoaId;
		result = 31 * result + status.hashCode();
		result = 31 * result + (certificado != null ? certificado.hashCode() : 0);
		return result;
	}
}