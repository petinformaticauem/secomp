package com.pet.secomp.model.evento;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class PresencaPK implements Serializable {

	@Id
	@Column(name = "presenca_periodo_id", nullable = false)
	private int periodoId;

	@Id
	@Column(name = "presenca_inscricao_id", nullable = false)
	private int inscricaoId;

	public int getPeriodoId() {
		return periodoId;
	}

	public void setPeriodoId(int periodoId) {
		this.periodoId = periodoId;
	}

	public int getInscricaoId() {
		return inscricaoId;
	}

	public void setInscricaoId(int inscricaoId) {
		this.inscricaoId = inscricaoId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof PresencaPK)) return false;

		PresencaPK that = (PresencaPK) o;

		if (periodoId != that.periodoId) return false;
		return inscricaoId == that.inscricaoId;
	}

	@Override
	public int hashCode() {
		int result = periodoId;
		result = 31 * result + inscricaoId;
		return result;
	}
}