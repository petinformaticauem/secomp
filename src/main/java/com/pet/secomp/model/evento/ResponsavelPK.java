package com.pet.secomp.model.evento;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class ResponsavelPK implements Serializable {
	@Id
	@Column(name = "responsavel_atividade_id", nullable = false)
	private int atividadeId;

	@Id
	@Column(name = "responsavel_pessoa_id", nullable = false)
	private int pessoaId;

	public int getAtividadeId() {
		return atividadeId;
	}

	public void setAtividadeId(int atividadeId) {
		this.atividadeId = atividadeId;
	}

	public int getPessoaId() {
		return pessoaId;
	}

	public void setPessoaId(int pessoaId) {
		this.pessoaId = pessoaId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof ResponsavelPK)) return false;

		ResponsavelPK that = (ResponsavelPK) o;

		if (atividadeId != that.atividadeId) return false;
		return pessoaId == that.pessoaId;
	}

	@Override
	public int hashCode() {
		int result = atividadeId;
		result = 31 * result + pessoaId;
		return result;
	}
}
