package com.pet.secomp.model.evento;

import com.pet.secomp.model.enums.TiposAtividade;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "Atividade")
public class Atividade {

	@Id
	@Column(name = "atividade_id", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@NotNull
	@Basic(optional = false)
	@Column(name = "atividade_nome", nullable = false)
	private String nome;

	@NotNull
	@Basic(optional = false)
	@Column(name = "atividade_descricao", nullable = false)
	private String descricao;

	@NotNull
	@Basic(optional = false)
	@Column(name = "atividade_tipo", nullable = false)
	@Enumerated(EnumType.STRING)
	private TiposAtividade tipo;

	@NotNull
	@Basic(optional = false)
	@Column(name = "atividade_local", nullable = false)
	private String local;

	@NotNull
	@Basic(optional = false)
	@Column(name = "atividade_inscricao_inicio", nullable = false)
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date inscricaoInicio;

	@NotNull
	@Basic(optional = false)
	@Column(name = "atividade_inscricao_fim", nullable = false)
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date inscricaoFim;

	@NotNull
	@Basic(optional = false)
	@Column(name = "atividade_custo", nullable = false)
	private int custo;

	@NotNull
	@Basic(optional = false)
	@Column(name = "atividade_vagas", nullable = false)
	private int vagas;

	@NotNull
	@Basic(optional = false)
	@Column(name = "atividade_carga_horaria", nullable = false)
	private int cargaHoraria;

	@NotNull
	@Basic(optional = false)
	@Column(name = "atividade_evento_id", nullable = false)
	private int eventoId;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public TiposAtividade getTipo() {
		return tipo;
	}

	public void setTipo(TiposAtividade tipo) {
		this.tipo = tipo;
	}

	public String getLocal() {
		return local;
	}

	public void setLocal(String local) {
		this.local = local;
	}

	public Date getInscricaoInicio() {
		return inscricaoInicio;
	}

	public void setInscricaoInicio(Date inscricaoInicio) {
		this.inscricaoInicio = inscricaoInicio;
	}

	public Date getInscricaoFim() {
		return inscricaoFim;
	}

	public void setInscricaoFim(Date inscricaoFim) {
		this.inscricaoFim = inscricaoFim;
	}

	public int getCusto() {
		return custo;
	}

	public void setCusto(int custo) {
		this.custo = custo;
	}

	public int getVagas() {
		return vagas;
	}

	public void setVagas(int vagas) {
		this.vagas = vagas;
	}

	public int getCargaHoraria() {
		return cargaHoraria;
	}

	public void setCargaHoraria(int cargaHoraria) {
		this.cargaHoraria = cargaHoraria;
	}

	public int getEventoId() {
		return eventoId;
	}

	public void setEventoId(int eventoId) {
		this.eventoId = eventoId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Atividade atividade = (Atividade) o;

		if (id != atividade.id) return false;
		if (custo != atividade.custo) return false;
		if (vagas != atividade.vagas) return false;
		if (cargaHoraria != atividade.cargaHoraria) return false;
		if (eventoId != atividade.eventoId) return false;
		if (nome != null ? !nome.equals(atividade.nome) : atividade.nome != null) return false;
		if (descricao != null ? !descricao.equals(atividade.descricao) : atividade.descricao != null) return false;
		if (tipo != atividade.tipo) return false;
		if (local != null ? !local.equals(atividade.local) : atividade.local != null) return false;
		if (inscricaoInicio != null ? !inscricaoInicio.equals(atividade.inscricaoInicio) : atividade.inscricaoInicio != null)
			return false;
		return inscricaoFim != null ? inscricaoFim.equals(atividade.inscricaoFim) : atividade.inscricaoFim == null;
	}

	@Override
	public int hashCode() {
		int result = id;
		result = 31 * result + (nome != null ? nome.hashCode() : 0);
		result = 31 * result + (descricao != null ? descricao.hashCode() : 0);
		result = 31 * result + (tipo != null ? tipo.hashCode() : 0);
		result = 31 * result + (local != null ? local.hashCode() : 0);
		result = 31 * result + (inscricaoInicio != null ? inscricaoInicio.hashCode() : 0);
		result = 31 * result + (inscricaoFim != null ? inscricaoFim.hashCode() : 0);
		result = 31 * result + custo;
		result = 31 * result + vagas;
		result = 31 * result + cargaHoraria;
		result = 31 * result + eventoId;
		return result;
	}
}