package com.pet.secomp.model.evento;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "Periodo")
public class Periodo {

	@Id
	@Column(name = "periodo_id", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Basic(optional = false)
	@Column(name = "periodo_atividade_id", nullable = false)
	private int atividadeId;

	@NotNull
	@Basic(optional = false)
	@Column(name = "periodo_nome", nullable = false)
	private String nome;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAtividadeId() {
		return atividadeId;
	}

	public void setAtividadeId(int atividadeId) {
		this.atividadeId = atividadeId;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Periodo)) return false;

		Periodo periodo = (Periodo) o;

		if (id != periodo.id) return false;
		if (atividadeId != periodo.atividadeId) return false;
		return nome.equals(periodo.nome);
	}

	@Override
	public int hashCode() {
		int result = id;
		result = 31 * result + atividadeId;
		result = 31 * result + nome.hashCode();
		return result;
	}
}