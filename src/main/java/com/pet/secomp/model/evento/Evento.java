package com.pet.secomp.model.evento;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "Evento")
public class Evento {

	@Id
	@Column(name = "evento_id", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@NotNull
	@Basic(optional = false)
	@Column(name = "evento_nome", nullable = false)
	private String nome;

	@Basic
	@Column(name = "evento_inicio")
	private Date inicio;

	@Basic
	@Column(name = "evento_fim")
	private Date fim;

	@Basic
	@Column(name = "evento_local")
	private String local;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getInicio() {
		return inicio;
	}

	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}

	public Date getFim() {
		return fim;
	}

	public void setFim(Date fim) {
		this.fim = fim;
	}

	public String getLocal() {
		return local;
	}

	public void setLocal(String local) {
		this.local = local;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Evento)) return false;

		Evento evento = (Evento) o;

		if (!id.equals(evento.id)) return false;
		if (!nome.equals(evento.nome)) return false;
		if (inicio != null ? !inicio.equals(evento.inicio) : evento.inicio != null) return false;
		if (fim != null ? !fim.equals(evento.fim) : evento.fim != null) return false;
		return local != null ? local.equals(evento.local) : evento.local == null;
	}

	@Override
	public int hashCode() {
		int result = id.hashCode();
		result = 31 * result + nome.hashCode();
		result = 31 * result + (inicio != null ? inicio.hashCode() : 0);
		result = 31 * result + (fim != null ? fim.hashCode() : 0);
		result = 31 * result + (local != null ? local.hashCode() : 0);
		return result;
	}
}