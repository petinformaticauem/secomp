package com.pet.secomp.model;

import com.pet.secomp.model.enums.Curso;
import com.pet.secomp.model.enums.Role;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "Petiano")
public class Petiano {

	@Id
	@Column(name = "petiano_id", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@NotNull
	@Basic(optional = false)
	@Column(name = "petiano_curso", nullable = false)
	@Enumerated(EnumType.STRING)
	private Curso curso;

	@NotNull
	@Basic(optional = false)
	@Column(name = "petiano_role", nullable = false)
	@Enumerated(EnumType.STRING)
	private Role role;

	@NotNull
	@Basic(optional = false)
	@Column(name = "petiano_ingresso", nullable = false)
	private Integer ingresso;

	@NotNull
	@Basic
	@Column(name = "petiano_saida")
	private Integer saida;

	@NotNull
	@Basic
	@Column(name = "petiano_bio")
	private String bio;

	@NotNull
	@OneToOne
	@JoinColumn(name = "petiano_pessoa_id", unique = true)
	private Pessoa pessoa;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Integer getIngresso() {
		return ingresso;
	}

	public void setIngresso(Integer ingresso) {
		this.ingresso = ingresso;
	}

	public Integer getSaida() {
		return saida;
	}

	public void setSaida(Integer saida) {
		this.saida = saida;
	}

	public String getBio() {
		return bio;
	}

	public void setBio(String bio) {
		this.bio = bio;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Petiano)) return false;

		Petiano petiano = (Petiano) o;

		if (id != petiano.id) return false;
		if (curso != petiano.curso) return false;
		if (role != petiano.role) return false;
		if (!ingresso.equals(petiano.ingresso)) return false;
		if (saida != null ? !saida.equals(petiano.saida) : petiano.saida != null) return false;
		if (bio != null ? !bio.equals(petiano.bio) : petiano.bio != null) return false;
		return pessoa.equals(petiano.pessoa);
	}

	@Override
	public int hashCode() {
		int result = id;
		result = 31 * result + curso.hashCode();
		result = 31 * result + role.hashCode();
		result = 31 * result + ingresso.hashCode();
		result = 31 * result + (saida != null ? saida.hashCode() : 0);
		result = 31 * result + (bio != null ? bio.hashCode() : 0);
		result = 31 * result + pessoa.hashCode();
		return result;
	}
}
