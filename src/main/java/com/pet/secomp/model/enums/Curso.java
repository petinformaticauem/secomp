package com.pet.secomp.model.enums;

public enum Curso {
    CIENCIA_DA_COMPUTACAO, INFORMATICA, ENGENHARIA_DE_PRODUCAO_COM_ENFASE_EM_SOFTWARE
}