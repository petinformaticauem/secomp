package com.pet.secomp.model.enums;

public enum Role {
    PETIANO, TUTOR, EGRESSO, TUTOR_EGRESSO
}