package com.pet.secomp.model.enums;

public enum StatusInscricao {
	VALIDA, INVALIDA, CONFIRMADA
}
