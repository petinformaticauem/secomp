CREATE TABLE IF NOT EXISTS evento (
  evento_id     INT(11)      NOT NULL AUTO_INCREMENT,
  evento_nome   VARCHAR(255) NOT NULL,
  evento_inicio DATE         NOT NULL,
  evento_fim    DATE         NOT NULL,
  evento_local  VARCHAR(255) NOT NULL,
  PRIMARY KEY (evento_id)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE IF NOT EXISTS atividade (
  atividade_id               INT(11)       NOT NULL AUTO_INCREMENT,
  atividade_nome             VARCHAR(255)  NOT NULL,
  atividade_descricao        VARCHAR(1023) NOT NULL,
  atividade_tipo             VARCHAR(16)   NOT NULL,
  atividade_local            VARCHAR(255)  NOT NULL,
  atividade_inscricao_inicio DATE          NOT NULL,
  atividade_inscricao_fim    DATE          NOT NULL,
  atividade_custo            INT(11)       NOT NULL,
  atividade_vagas            INT(11)       NOT NULL,
  atividade_carga_horaria    INT(11)       NOT NULL,
  atividade_status           VARCHAR(16)   NOT NULL,
  atividade_evento_id        INT(11)       NOT NULL,
  PRIMARY KEY (atividade_id)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE IF NOT EXISTS pessoa (
  pessoa_id         INT(11)      NOT NULL AUTO_INCREMENT,
  pessoa_nome       VARCHAR(255) NOT NULL,
  pessoa_email      VARCHAR(255),
  pessoa_password   VARCHAR(60)           DEFAULT NULL,
  pessoa_rg         VARCHAR(9),
  pessoa_cpf        VARCHAR(11),
  pessoa_nascimento DATE,
  pessoa_phone      VARCHAR(11),
  pessoa_address    VARCHAR(255),
  PRIMARY KEY (pessoa_id)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE IF NOT EXISTS inscricao (
  inscricao_atividade_id INT(11)     NOT NULL,
  inscricao_pessoa_id    INT(11)     NOT NULL,
  inscricao_status       VARCHAR(16) NOT NULL,
  inscricao_certificado  VARCHAR(255)         DEFAULT NULL,
  inscricao_frequencia   INT(11)              DEFAULT NULL,
  inscricao_id           INT         NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (inscricao_id)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE IF NOT EXISTS petiano (
  petiano_id        INT(11)      NOT NULL AUTO_INCREMENT,
  petiano_curso     VARCHAR(255) NOT NULL,
  petiano_role      VARCHAR(16)  NOT NULL,
  petiano_ingresso  INT(11)      NOT NULL,
  petiano_saida     INT(11)               DEFAULT NULL,
  petiano_bio       VARCHAR(255)          DEFAULT NULL,
  petiano_pessoa_id INT(11)      NOT NULL,
  PRIMARY KEY (petiano_id)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE IF NOT EXISTS responsavel (
  responsavel_atividade_id INT(11) NOT NULL,
  responsavel_pessoa_id    INT(11) NOT NULL,
  PRIMARY KEY (responsavel_atividade_id, responsavel_pessoa_id)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE IF NOT EXISTS periodo (
  periodo_id           INT          NOT NULL AUTO_INCREMENT,
  periodo_atividade_id INT          NOT NULL,
  periodo_nome         VARCHAR(255) NOT NULL,
  periodo_data         TIMESTAMP,
  PRIMARY KEY (periodo_id)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE IF NOT EXISTS presenca (
  presenca_periodo_id   INT NOT NULL,
  presenca_inscricao_id INT NOT NULL,
  PRIMARY KEY (presenca_periodo_id, presenca_inscricao_id)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

ALTER TABLE atividade ADD CONSTRAINT fk_atividade_evento_id FOREIGN KEY (atividade_evento_id) REFERENCES evento (evento_id);
ALTER TABLE atividade ADD INDEX evento_id_idx (atividade_evento_id ASC);

ALTER TABLE pessoa ADD UNIQUE INDEX pessoa_email_UNIQUE (pessoa_email ASC);
ALTER TABLE pessoa ADD UNIQUE INDEX pessoa_rg_UNIQUE (pessoa_rg ASC);
ALTER TABLE pessoa ADD UNIQUE INDEX pessoa_cpf_UNIQUE (pessoa_cpf ASC);

ALTER TABLE inscricao ADD CONSTRAINT fk_inscricao_atividade_id FOREIGN KEY (inscricao_atividade_id) REFERENCES atividade (atividade_id);
ALTER TABLE inscricao ADD CONSTRAINT fk_inscricao_pessoa_id FOREIGN KEY (inscricao_pessoa_id) REFERENCES pessoa (pessoa_id);
ALTER TABLE inscricao ADD INDEX atividade_id_idx (inscricao_atividade_id ASC);
ALTER TABLE inscricao ADD INDEX fk_inscricao_pessoa_id_idx (inscricao_pessoa_id ASC);
ALTER TABLE inscricao ADD UNIQUE INDEX inscricao_id_UNIQUE (inscricao_id ASC);

ALTER TABLE petiano ADD CONSTRAINT fk_petiano_pessoa_id FOREIGN KEY (petiano_pessoa_id) REFERENCES pessoa (pessoa_id);
ALTER TABLE petiano ADD INDEX pessoa_id_idx (petiano_pessoa_id ASC);
ALTER TABLE petiano ADD UNIQUE INDEX petiano_pessoa_id_UNIQUE (petiano_pessoa_id ASC);

ALTER TABLE responsavel ADD CONSTRAINT fk_responsavel_atividade_id FOREIGN KEY (responsavel_atividade_id) REFERENCES atividade (atividade_id);
ALTER TABLE responsavel ADD CONSTRAINT fk_responsavel_pessoa_id FOREIGN KEY (responsavel_pessoa_id) REFERENCES pessoa (pessoa_id);
ALTER TABLE responsavel ADD INDEX responsavel_atividade_id_idx (responsavel_atividade_id ASC);
ALTER TABLE responsavel ADD INDEX responsavel_pessoa_id_idx (responsavel_pessoa_id ASC);

ALTER TABLE periodo ADD CONSTRAINT fk_periodo_atividade_id FOREIGN KEY (periodo_atividade_id) REFERENCES atividade (atividade_id);
ALTER TABLE periodo ADD INDEX fk_periodo_atividade_id_idx (periodo_atividade_id ASC);

ALTER TABLE presenca ADD CONSTRAINT fk_presenca_periodo_id FOREIGN KEY (presenca_periodo_id) REFERENCES periodo (periodo_id);
ALTER TABLE presenca ADD CONSTRAINT fk_presenca_inscricao_id FOREIGN KEY (presenca_inscricao_id) REFERENCES inscricao (inscricao_id);
ALTER TABLE presenca ADD INDEX fk_presenca_inscricao_id_idx (presenca_inscricao_id ASC);