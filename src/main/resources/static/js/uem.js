var width = $(window).width();
var done = false;
$(window).on('resize', function(){
    if($(this).width() !== width){
        width = $(this).width();
        if(width < 700 && done !== true) {
            $("<br />").insertAfter(".hour");
            $('.hour').removeAttr('style');
            done = true;
        }
    }
});